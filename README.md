# bugzilla-kanban
Display Bugzilla bugs on a kanban board

# To get the Jenkins Builds working
Requires a proxy to be running on the local machine on localhost:8080 pointing at mfk-ci0

With npm: 
```
npx get-fwd mfk-ci0
```

Due to cors an https site can't make requests to an http origin for security, however localhost is an exception to this rule

if you cant run the proxy on 8080, change localStorage.JENKINS_PORT to a different port
if mfk-ci0 is no longer the jenkins host, update the param to get-fwd
