import { BugsStore } from "./Bugs.store";
import { AppStore } from "./App.store";
import { refreshBugs } from '../Bug/BugLoader';
import { updateBug } from '../Bug/BugUpdater';
import { COLUMNS, getColumn, STATUS_COLUMNS } from "./Columns";

export const DEFAULT_PRIORITY_GAP = 1000;

const BugPriorityService = {
    DEFAULT_PRIORITY_GAP,
    watchBugsForMissingPriorities() {
        BugsStore.on('state', ({ current, changed }) => {
            if (changed.bugsByBvu && !AppStore.get().updatingPriorities) {
                const allUpdates = current.bugsByBvu.map(bvuBugs =>
                    STATUS_COLUMNS.map((column) => updateMissingPrioritiesForBVUStatus(bvuBugs.bugs, column))
                        .reduce((acc, updatePromises) => acc.concat(updatePromises), [])
                ).reduce((acc, byBvuUpdatePromises) => acc.concat(byBvuUpdatePromises), []);

                AppStore.set({ updatingPriorities: allUpdates.length });

                Promise.all(allUpdates)
                    .then((results) => results.length ? refreshBugs() : Promise.resolve())
                    .then(() => {
                        AppStore.set({ updatingPriorities: false });
                    }).catch(err => {
                        console.log(err);
                        AppStore.set({ updatingPrioritiesError: err });
                    });
            }
        });
    },
    moveBug(bug, destBug, before) {
        if (bug.id == destBug.id) return;

        const bvu = bug.blocks[0];
        const sortedBugsWithStatus = this.getBugsWithStatus(bvu, getColumn(bug)).sort((b1, b2) => b2.data.priority - b1.data.priority);
        const destIndex = sortedBugsWithStatus.map(b => b.id).indexOf(destBug.id);
        const bugIndex = sortedBugsWithStatus.map(b => b.id).indexOf(bug.id);

        if (bugIndex + (before ? 1 : -1) == destIndex) {
            // Do nothing, position has not changed
            return;
        }

        let priority;
        if (destIndex == 0 && before) {
            // Top of List
            priority = destBug.data.priority + DEFAULT_PRIORITY_GAP;
        } else if (destIndex == sortedBugsWithStatus.length - 1 && !before) {
            // Bottom of List
            priority = destBug.data.priority - DEFAULT_PRIORITY_GAP;
        } else {
            const otherBugIndex = destIndex + (before ? -1 : 1);
            const otherBug = sortedBugsWithStatus[otherBugIndex];

            priority = Math.round((destBug.data.priority + otherBug.data.priority) / 2)
        }

        updateBug(bug.id, { data: Object.assign({}, bug.data, { priority }) });
    },
    getBugsWithStatus(bvu, column) {
        const { bugsByBvu } = BugsStore.get();
        const bugsInBvu = bugsByBvu.find(bbbvu => bbbvu.bvu == bvu).bugs;
        return columnToByFunc[column](bugsInBvu);
    },
    getLowestPriority(bvu, column) {
        const bugsInColumn = this.getBugsWithStatus(bvu, column);

        return bugsInColumn
            .map((b) => b.data.priority)
            .filter(Boolean)
            .concat([0])
            .reduce((a, b) => Math.min(a, b));
    },
    calcPriorityForStatusUpdate(bvu, column) {
        return this.getLowestPriority(bvu, column) - BugPriorityService.DEFAULT_PRIORITY_GAP;
    },
};

export default BugPriorityService;

const columnToByFunc = {
    [COLUMNS.NEW]: BugsStore.byNew,
    [COLUMNS.ASSIGNED]: BugsStore.byAssigned,
    [COLUMNS.RESOLVED]: BugsStore.byResolved,
    [COLUMNS.VERIFIED]: BugsStore.byVerified,
    [COLUMNS.CLOSED]: BugsStore.byClosed,
};

function updateMissingPrioritiesForBVUStatus(bugs, column) {
    const bugsInColumn = columnToByFunc[column](bugs);
    const bugsWithoutPriority = bugsInColumn.filter(b => b.data.priority == null);

    if (!bugsWithoutPriority.length) {
        return [];
    }

    const lowestPriority = bugsInColumn
        .map((b) => b.data.priority)
        .filter(Boolean)
        .concat([0])
        .reduce((a, b) => Math.min(a, b));

    return bugsWithoutPriority
        .map((bug, i) => [bug, lowestPriority - (i * DEFAULT_PRIORITY_GAP)])
        .map(([bug, priority]) => updateBugPriority(bug, priority));
}

function updateBugPriority(bug, priority) {
    const mods = { data: Object.assign({}, bug.data, { priority }) };
    return updateBug(bug.id, mods);
}
