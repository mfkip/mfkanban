
let knownStampsById;
let knownStamps;
function updateKnownStamps() {
    localStorage.setItem('bugzillaStamps', JSON.stringify(Array.from(knownStamps)));
}

export function pushStamp(stamp) {
    pushStamps([stamp]);
}

export function pushStamps(stamps) {
    getKnownStamps();
    const unknownStamps = stamps.filter(stamp => !knownStampsById.has(stamp.icon));
    if (unknownStamps.length) {
        knownStamps = knownStamps.concat(unknownStamps).sort((a,b) => (a.icon || '').localeCompare(b.icon));
        unknownStamps.forEach(s => knownStampsById.add(s.icon));
        updateKnownStamps();
    }
}

export function getKnownStamps() {
    if (!knownStamps) {
        knownStamps = JSON.parse(localStorage.getItem('bugzillaStamps') || '[]');
        knownStampsById = new Set(knownStamps.map(stamp => stamp.icon));
    }
    return knownStamps;
}
