import { getLogin } from './login.service';

export function getKnownUsers() {
    return getLogin()
        .then(({ token }) => fetch(`${MFK_BUGZILLA}/rest/user?match=foo&token=${token}`))
        .then(res => res.status >= 200 && res.status < 300
            ? res.json()
            : res.json().then(errBody => Promise.reject(errBody)));;
}

export function getProducts() {
    return fetch(`${MFK_BUGZILLA}/rest/product?type=selectable`)
        .then(res => res.json());
}

export function getBugs(ids) {
    if (ids && ids.length) {
        return Promise.resolve(window.bugsStore.get().bugs.filter(b => ids.includes(b.id)));
    } else {
        return Promise.resolve(window.bugsStore.get().bugs);
    }
}

export function getBug(id) {
    return getBugs([id])
        .then(bugs => bugs[0]);
}

export function getBugComments(id) {
    return fetch(`${MFK_BUGZILLA}/rest/bug/${id}/comment`)
        .then(res => res.status >= 200 && res.status < 300 ? res.json() : Promise.reject())
        .then(data => data.bugs[id].comments);
}

export function createBugComment(bugId, comment, token) {
    getBug(bugId)
    .then(bug => window.bugsStore.putBug(bug.id, { ...bug, comments: (bug.comments || []).concat({
        id: Date.now(),
        text: comment,
        creator: window.AppStore.get().username,
        creation_time: Date.now(),
    }) }));
}

export function bugzillaLogin(username, password) {
    return Promise.resolve({});
}
