import { bugzillaLogin } from './bugzilla.service';
import { AppStore } from './App.store';
import { navigate } from '../router/router.service';

export function login(username, password) {
    return bugzillaLogin(username)
        .then(() => {
            AppStore.set({ isLoggedIn: true, username });
            navigate('/board?reload');
        });
}

export function logout() {
    AppStore.set({ isLoggedIn: false, username: '', initialized: false });
}

export function getLogin() {
    const { username, token } = AppStore.get();

    return Promise.resolve({ username, token });
}
