
let knownUsers;
function updateKnownUsers() {
    localStorage.setItem('bugzillaUsers', JSON.stringify(Array.from(knownUsers || []).sort()));
}

export function pushUser(user) {
    pushUsers([user]);
}

export function pushUsers(users) {
    getKnownUsers();
    const beforeUpdatesSize = knownUsers.size;
    users.forEach(user => knownUsers.add(user));
    if (knownUsers.size !== beforeUpdatesSize) {
        updateKnownUsers();
    }
}

export function getKnownUsers() {
    if (!knownUsers) {
        knownUsers = new Set(JSON.parse(localStorage.getItem('bugzillaUsers') || '[]'));
    }
    return Array.from(knownUsers);
}
