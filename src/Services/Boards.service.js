import { AppStore } from './App.store';

const boards = JSON.parse(localStorage.boards || JSON.stringify([{
    id: '1',
    bvus: [],
    name: 'Welcome Board',
    swarmKey: 'L2tleS9zd2FybS9wc2svMS4wLjAvCi9iYXNlMTYvCjIxNmI5OWYxZWExMzAwYTExYWM4ZGFkODIxMTg3NzEyN2I1NDM1MjgwY2IxOTExYTcxN2FjNTJmZGNiNDE3M2Q'
}]));

export const BoardsService = {
    async get() {
        AppStore.set({ boards });

        return boards;
    },
    async update(id, name, bvus) {
        const boardToUpdate = boards.find(b => b.id === id);
        if (boardToUpdate) {
            Object.assign(boardToUpdate, { bvus, name });
        } else {
            boards.push({
                id: id || Date.now().toString(),
                name, bvus,
                swarmKey: MFKIP.createSwarmKey(),
            });
        }

        localStorage.boards = JSON.stringify(boards);
        AppStore.set({ boards });
        return '';
    },
    import(board) {
        board.bvus = [];

        boards.push(board);
        localStorage.boards = JSON.stringify(boards);
        AppStore.set({ boards });
    }
};

BoardsService.get();

window.BoardsService = BoardsService;
