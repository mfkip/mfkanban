import { Store } from "svelte/store";
import { COLUMNS, getColumn } from './Columns';
import { BoardsService } from './Boards.service';
import { pushUsers } from './Users.service';
import { pushStamps } from './Stamps.service';
import { Observable } from "rxjs";
import { distinctUntilChanged, map, filter, startWith, tap } from "rxjs/operators";

const REALLY_SMALL_NEGATIVE = -1e10;

const withColumn = (bugs, column) => {
    if (bugs) {
        return bugs.filter(bug => {
            return getColumn(bug) === column;
        }).sort((b1, b2) => (b2.data.priority == null ? REALLY_SMALL_NEGATIVE : b2.data.priority) - (b1.data.priority == null ? REALLY_SMALL_NEGATIVE : b1.data.priority));
    }

    return [];
};

let dbResolver;

let bugsDb$ = new Promise(res => dbResolver = res);
const boardId = localStorage.appState_boardId;
if (boardId) {
    BoardsService.get().then(boards => {
        const board = boards.find(b => b.id == boardId);

        const bugsMfkip = new MFKIP({
            encodedKey: board.swarmKey,
            listenAddresses: [
                '/dns4/guarded-anchorage-63004.herokuapp.com//tcp/443/wss/p2p-webrtc-star'
            ],
            appId: 'mfkanban',
            minConnections: 5,
            maxConnections: 20,
            partitions: [boardId],
        });

        bugsMfkip.openDb('bugs', 'keyvalue')
            .then(db => dbResolver(db));

        bugsMfkip.start()
            .then(() => console.log('bugsDb started'));
    })

}

function checkForUsersAndStamps(bugs) {
    bugs.forEach(bug => {
        if (bug.data.assignees) {
            pushUsers(bug.data.assignees);
        }
    
        if (bug.data.stamps) {
            pushStamps(bug.data.stamps);
        }
    })
}

class BugStore extends Store {

    constructor() {
        super({ bugs: [] });
        bugsDb$.then(db => {
            db.events.on('mfkip.change', () => {
                this.set({ bugs: Object.values(db.all) });
                checkForUsersAndStamps(this.get().bugs);
            });
        });
    }

    byNew(bugs) {
        return withColumn(bugs, COLUMNS.NEW);
    }
    byAssigned(bugs) {
        return withColumn(bugs, COLUMNS.ASSIGNED);
    }
    byResolved(bugs) {
        return withColumn(bugs, COLUMNS.RESOLVED);
    }
    byVerified(bugs) {
        return withColumn(bugs, COLUMNS.VERIFIED);
    }
    byClosed(bugs) {
        return withColumn(bugs, COLUMNS.CLOSED);
    }

    async putBug(id, bug) {
        return (await bugsDb$).put(id.toString(), { ...bug, id });
    }

    async getBug(id) {
        return (await bugsDb$).get(id.toString());
    }

    async delBug(id) {
        return (await bugsDb$).del(id.toString());
    }

    comments$(bugId) {
        return new Observable(obs => {
            const listener = this.on('update', ({ changed, current }) => {
                const bug = (current.bugs || []).find(b => b.id === bugId);
                obs.next(bug);
            });

            obs.next(this.get().bugs.find(b => b.id === bugId));

            return listener.cancel;
        }).pipe(
            filter(Boolean),
            startWith({}),
            distinctUntilChanged((a, b) => (a.comments || []).length === (b.comments || []).length),
            map(b => b.comments || []),
        )
    }
}

export const BugsStore = new BugStore();

BugsStore.compute(
    'bugsByBvu',
    ['bugs'],
    (bugs) => {
        const grouped = (bugs || []).reduce((acc, bug) => {
            acc[bug.blocks[0]] = (acc[bug.blocks[0]] || []).concat(bug);
            return acc;
        }, {});

        return Object.keys(grouped).reduce((acc, key) => {
            acc.push({ bvu: key, bugs: grouped[key] });
            return acc;
        }, []);
    }
);

window.bugsStore = BugsStore;