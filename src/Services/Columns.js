export function getColumn(bug) {
    return statusToColumn[bug.status.toUpperCase()];
}

const statusToColumn = {
    NEW: "NEW",
    REOPENED: "NEW",
    ASSIGNED: "ASSIGNED",
    RESOLVED: "RESOLVED",
    VERIFIED: "VERIFIED",
    CLOSED: "CLOSED",
}

export const COLUMNS = {
    NEW: "NEW",
    ASSIGNED: "ASSIGNED",
    RESOLVED: "RESOLVED",
    VERIFIED: "VERIFIED",
    CLOSED: "CLOSED",
}

export const STATUS_COLUMNS = Object.keys(COLUMNS).map(key => COLUMNS[key]);
