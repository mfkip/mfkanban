import { Store } from 'svelte/store';

export const AppStore = new Store({
    loadBy: 'bvus',
    productId: loadField('productId'),
    productName: loadField('productName'),
    milestoneId: loadField('milestoneId'),
    milestoneName: loadField('milestoneName'),
    componentName: loadField('componentName'),
    username: loadField('username'),
    token: loadField('token'),
    isLoggedIn: parseBool(loadField('isLoggedIn')),
    expandedColumns: JSON.parse(loadField('expandedColumns') || '[]'),
    initialized: false,
    updatingPriorities: false,
    boardId: loadField('boardId'),
    lightsOn: JSON.parse(loadField('lightsOn') || 'false'),
    boards: [],
});

AppStore.compute('boardBvuList', ['boards', 'boardId'], (boards, boardId) => {
    const board = boards.find(b => b.id == boardId);
    return !board ? null : (board.bvus || null);
});
AppStore.compute('title', ['boards', 'boardId'], (boards, boardId) => {
    const board = boards.find(b => b.id == boardId);
    return board ? board.name : '';
});

AppStore.on('state', ({ current, changed, previous }) => {
    if (changed.isLoggedIn) {
        localStorage.setItem('appState_isLoggedIn', current.isLoggedIn ? 'true' : 'false');
    }

    saveStringChanges([
        'token',
        'username',
        'loadBy',
        'productId',
        'milestoneId',
        'productName',
        'milestoneName',
        'componentName',
        'lightsOn',
        'boardId',
    ], changed, current);

    if (changed.expandedColumns) {
        localStorage.setItem('appState_expandedColumns', JSON.stringify(current.expandedColumns));
    }
    
    if (changed.boardBvuList) {
        localStorage.setItem('appState_boardBvuList', JSON.stringify(current.boardBvuList));
    }
});

function saveStringChanges(fields, changed, current) {
    if (changed.boardId) {
        console.log(current.boardId);
    }
    fields.forEach(field => {
        if (changed[field]) {
            localStorage.setItem('appState_' + field, current[field]);
        }
    });
}

function loadField(field) {
    return localStorage.getItem('appState_' + field);
}

function parseBool(val) {
    return val == 'true';
}

window.AppStore = AppStore;
