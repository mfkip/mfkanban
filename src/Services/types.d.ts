interface Bug {
    id: number;
    data: {
        assignees?: any[];
        stamps?: any[];
        priority?: number;
    };
    summary: string;
    cf_summary: string;
    cf_real_acceptance_criteria: string;
    cf_to_do: string;
    blocks: [number];
    comments: any[];
    status: 'NEW' | 'REOPENED' | 'ASSIGNED' | 'RESOLVED' | 'VERIFIED' | 'CLOSED';
    resolution?: 'FIXED';
}

interface UpdateBugPayload extends Bug {
    comment: {
        body: string;
        is_markdown: boolean;
    }
}

interface Comment {
    comment: string;
    is_private: boolean;
    is_markdown: boolean;
}