import '@mfkip/web-lib';
import App from './App.html';
import BugPriorityService from './Services/BugPriority.service';
import { AppStore } from './Services/App.store';

BugPriorityService.watchBugsForMissingPriorities();

localStorage.removeItem('bugzillaLogin');
localStorage.removeItem('lastProductAndMilestone');

function checkForSwSupport() {
	if (!('serviceWorker' in navigator)) {
		throw new Error('No Service Worker support!')
	}
}

async function registerServiceWorker() {
	return await navigator.serviceWorker.register('./sw.js', { scope: './' });
}

(async () => {
	if (!window.dev) {
		checkForSwSupport();
		await registerServiceWorker();
	}
})()

window.addEventListener('beforeinstallprompt', e => {
	AppStore.set({ installPrompt: e });
});

window.addEventListener('appinstalled', e => {
	console.log('window.appinstalled', e);
	AppStore.set({ installPrompt: null });
});

const app = new App({
	target: document.body,
});

window.app = app;

export default app;