export function getStoragePic(user) {
    return localStorage.getItem('pic_' + user);
}

const USER_IMAGES_VERSION = 6;

function upgrade() {
    const versionText = localStorage.getItem('pic_version');
    let versionNumber = versionText ? parseInt(versionText, 10) : 0;
    
    if (versionNumber < USER_IMAGES_VERSION) {
        window['USER_IMAGES_VERSION'] = USER_IMAGES_VERSION;
        loadUpgradeScript();
    }
}

function loadUpgradeScript() {
    const script = document.createElement('script');
    script.src = 'UpgradeUserImages.js';
    script.type = 'text/javascript';

    document.head.appendChild(script);
}

// Invoked first time someone requires this file
// upgrade();
