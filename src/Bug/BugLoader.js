import debounce from 'lodash.debounce';
import { BugsStore } from '../Services/Bugs.store';
import { getBugs } from '../Services/bugzilla.service';
import { TimeToWork } from '../TimeToWork';

document.addEventListener('visibilitychange', () => reloadAllBugs());
let loadBugsTimeout = null
let reloadAllBugs = () => {};

const commonLoad = (reloadAllBugsValue, bugsGetter) => {
    reloadAllBugs = reloadAllBugsValue;
    
    if (document.hidden || !TimeToWork.isIt()) {
        console.log('Skipping board Update');
        loadBugsTimeout = setTimeout(reloadAllBugs, 1000 * 60 * 1);
        return;
    }

    console.log('Updating Board');
    if (loadBugsTimeout) {
        clearTimeout(loadBugsTimeout);
    }

    return bugsGetter()
        .then(() => loadBugsTimeout = setTimeout(reloadAllBugs, 1000 * 60 * 1));
}

export const refreshBugs = () => (() => reloadAllBugs())();

export const loadBugsByBvus = () => {};
// debounce((boardBvuList) => commonLoad(() => loadBugsByBvus(boardBvuList), () => {
//     return getBugs(boardBvuList)
//         .then(bvuBugs => getBugs(bvuBugs.map(bvu => bvu.depends_on || []).flat()))
//         .then(bugs => BugsStore.set({ bugs }));
//     }
// ));