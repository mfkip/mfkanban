import { BugsStore } from '../Services/Bugs.store';
import { onUpdateListeners } from './onUpdateListeners';
import isEqual from 'lodash/isEqual';

export function updateBug(id, changes) {
    const changedBug = findChangedBug(id);
    if (!changedBug) {
        return BugsStore.putBug(id, changes);
    }

    const bugBeforeChanges = Object.assign({}, changedBug);
    onUpdateListeners.forEach(listener => {
        changes = listener(changes, bugBeforeChanges);
    });
    
    const bugAfterChanges = Object.assign({}, bugBeforeChanges, changes);

    const isSame = isEqual(bugBeforeChanges, bugAfterChanges);

    return isSame
        ? Promise.resolve()
        : BugsStore.putBug(bugAfterChanges.id, bugAfterChanges);
}

function findChangedBug(id) {
    return BugsStore.get().bugs.find(bug => bug.id === id)
}