import { getColumn, STATUS_COLUMNS } from '../../Services/Columns';

export function bugMovedForwardNotFromNew(changes, bugBeforeChanges) {
    const data = Object.assign({}, bugBeforeChanges.data, changes.data);
    if(hasBugMovedForwardNotFromNew(changes, bugBeforeChanges)) {
        data.assignees = [];
    }

    return Object.assign({}, changes, { data });
}

function hasBugMovedForwardNotFromNew(changes, bugBeforeChanges) {
    if (!changes.status) return false;
    const changesColumnIndex = STATUS_COLUMNS.indexOf(getColumn(changes));
    const beforeChangesColumnIndex = STATUS_COLUMNS.indexOf(getColumn(bugBeforeChanges));
    return beforeChangesColumnIndex != 0 
        && changesColumnIndex > beforeChangesColumnIndex;
}