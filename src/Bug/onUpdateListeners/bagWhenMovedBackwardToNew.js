import { getColumn, COLUMNS } from '../../Services/Columns';

export function bagWhenMovedBackwardToNew(changes, bugBeforeChanges) {
    const resultingChanges = Object.assign({}, changes);
    if (changes.status) {
        const changesColumn = getColumn(changes);
        const changedBugColumn = getColumn(bugBeforeChanges);
        if (changesColumn == COLUMNS.NEW && changedBugColumn != changesColumn && changedBugColumn != COLUMNS.NEW) {
            resultingChanges.status = 'REOPENED';
        }
    }

    return resultingChanges;
}