import { bagWhenMovedBackwardToNew } from "./bagWhenMovedBackwardToNew";
import { bugMovedForwardNotFromNew } from "./bugMovedForwardNotFromNew";
import { setAssignedDateWhenEnteringAssignedFromNew } from "./setAssignedDateWhenEnteringAssignedFromNew";

export const onUpdateListeners = [
    bagWhenMovedBackwardToNew,
    bugMovedForwardNotFromNew,
    setAssignedDateWhenEnteringAssignedFromNew,
]