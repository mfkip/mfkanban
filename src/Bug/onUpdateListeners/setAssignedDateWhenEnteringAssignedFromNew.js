import { getColumn, COLUMNS } from '../../Services/Columns';

export function setAssignedDateWhenEnteringAssignedFromNew(changes, bugBeforeChanges) {
    const data = Object.assign({}, bugBeforeChanges.data, changes.data);

    if(hasBugMovedToAssignedFromNew(changes, bugBeforeChanges)) {
        console.log('New -> Assigned');
    }

    return Object.assign({}, changes, { data });
}

function hasBugMovedToAssignedFromNew(changes, bugBeforeChanges) {
    if (!changes.status) {
        return false;
    }
    
    const to = getColumn(changes);
    const from = getColumn(bugBeforeChanges);

    return to == COLUMNS.ASSIGNED && from == COLUMNS.NEW;
}