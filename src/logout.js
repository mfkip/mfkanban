import { navigate } from "./router/router.service";
import { logout } from "./Services/login.service";


export function logoutAndNavigate() {
    logout();
    navigate('/');
}