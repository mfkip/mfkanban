export const StatusColors = {
    NEW: '#cb80eb',
    ASSIGNED: '#df768f',
    RESOLVED: '#ff9f76',
    VERIFIED: '#80d5c5',
    CLOSED: '#6694ff',
    DEFAULT: '#6f7090',
}

export const Status = {
    NEW: 'NEW',
    ASSIGNED: 'ASSIGNED',
    RESOLVED: 'RESOLVED',
    VERIFIED: 'VERIFIED',
    CLOSED: 'CLOSED',
}