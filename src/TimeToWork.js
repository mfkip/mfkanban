export const TimeToWork = {
    isIt() {
        return isForcedTimeToWork() || (!isWeekend() && isInWorkHours());
    }
}

function isWeekend() {
    const day = new Date().getDay();
    return day !== 6 && day !== 7;
}

function isInWorkHours() {
    // 6:30am to 6pm

    const workStart = new Date();
    workStart.setHours(6, 30);

    const workEnd = new Date();
    workEnd.setHours(18, 0);

    const now = new Date();
    return now > workStart && now < workEnd;
}

function isForcedTimeToWork() {
    return true;
    // return localStorage.FORCE_WORKTIME && localStorage.FORCE_WORKTIME !== 'false';
}