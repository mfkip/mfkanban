import { JenkinsService } from "./Jenkins.service"
import { JenkinsStore } from "./Jenkins.store"
import { TimeToWork } from '../TimeToWork';

let runningInterval;

export const JenkinsTimer = {
    start() {
        if (!runningInterval) {
            this.update();

            runningInterval = setInterval(this.update, 10000);
        }
    },
    stop() {
        if (runningInterval) {
            clearInterval(runningInterval);
        }
    },
    update() {
        if (TimeToWork.isIt()) {
            JenkinsService.getBuilds().then(remoteJobs => JenkinsStore.set({ remoteJobs }));
        }
    }
}