export const JenkinsService = {
    getBuilds() {
        return fetch(`http://localhost:${localStorage.JENKINS_PORT || '8080'}/api/json?pretty=true`)
            .then(res => res.json())
            .then(jsonRes => jsonRes.jobs);
    }
}