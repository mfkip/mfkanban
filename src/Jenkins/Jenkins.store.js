import { Store } from "svelte/store";

class _JenkinsStore extends Store {
}

export const JenkinsStore = new _JenkinsStore({
    jobs: loadJsonArrayField('jobs'),
    remoteJobs: [],
    selectedJobs: [],
});

JenkinsStore.on('state', ({ current, changed, previous }) => {
    if(changed.jobs) {
        localStorage.setItem('jenkinsState_jobs', JSON.stringify(current.jobs));
    }
});

function loadJsonArrayField(field) {
    return JSON.parse(localStorage.getItem('jenkinsState_' + field) || '[]');
}
