import { AppStore } from "../Services/App.store";
import { navigate } from "./router.service";

export function isLoggedInGuard() {
    const { isLoggedIn } = AppStore.get();

    if (!isLoggedIn) {
        navigate('/login');
    }

    return isLoggedIn;
}