import { navigate } from "./router.service";

export function redirectGuard(redirectPath) {
    return () => {
        navigate(redirectPath);
        return false;
    };
}