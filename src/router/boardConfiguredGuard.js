import { navigate } from "./router.service";
import { AppStore } from "../Services/App.store";

export function boardConfiguredGuard() {
    const { boardId } = AppStore.get();

    if (!boardId) {
        navigate('/board/config');
    }

    return !!boardId;
}