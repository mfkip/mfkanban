import Navigo from 'navigo';

const router = new Navigo('/#/', true);
router.notFound(() => { 
    console.log('Route not found');
    router.navigate('/');
});

let isFirstBackCap = true;
const backCaptureRoute = {
    path: '/back-cap', execute: (ar) => {
        if (ar.query && isFirstBackCap) {
            router.navigate(ar.query);
        } else {
            router.navigate('/');
        }

        isFirstBackCap = false;
    }
}

let activatedRoute = null;
export function withRoutes(routes, setView) {
    routes.concat(backCaptureRoute).forEach(route => {
        router.on(route.path, (params, query) => {
            activatedRoute = { params, query, path: route.path };
            
            if (route.execute) {
                route.execute(activatedRoute);
            } else {
                if (route.pre) {
                    if (route.pre()) {
                        setView(route.component, route.overlay);
                    }
                } else {
                    setView(route.component, route.overlay);
                }
            }
        });
    });

    router.resolve();
    const destination = location.hash.split('#').slice(-1)[0];
    if (destination) {
        router.navigate('/back-cap?' + destination);
    } else {
        router.navigate('/back-cap');
    }
}

export function getActivatedRoute() {
    return activatedRoute;
}

export function navigate(url) {
    router.navigate(url);
}

export function back() {
    window.history.back();
}

export function pause() {
    router.pause();
}

export function resume() {
    router.resume();
}
