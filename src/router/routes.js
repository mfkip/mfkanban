import KanbanBoard from '../KanbanBoard/KanbanBoard';
import LoginView from '../Login';
import InitWizard from '../InitWizard/InitWizard';
import BugView from '../Bug/BugView';
import { isLoggedInGuard } from './isLoggedInGuard';
import { boardConfiguredGuard } from './boardConfiguredGuard';
import { redirectGuard } from './redirectGuard';
import { logoutAndNavigate } from '../logout';
import ImportBoard from '../ImportBoard';

export default [
    { path: '/', execute: redirectGuard('/board') },
    { path: '/import', component: ImportBoard },
    { path: '/login', component: LoginView },
    { path: '/logout', execute: logoutAndNavigate },
    { path: '/board/config', component: InitWizard, pre: isLoggedInGuard  },
    { path: '/board', component: KanbanBoard, pre: () => isLoggedInGuard() && boardConfiguredGuard() },
    { path: '/bug/:id', component: KanbanBoard, overlay: BugView, pre: () => isLoggedInGuard() && boardConfiguredGuard() }
]
