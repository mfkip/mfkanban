import { GitlabStore } from './Gitlab.store';

export const GitlabService = {
    getMfkProjects: (token) => {
        
        /*
            Currently this request will get the 100 most recently updated projects, if more than 100 
            is necessary in the future we'll need to implement some pagination and report back a total count
        */

        return fetch(
            `https://gitlab.asynchrony.com/api/v4/groups/71/projects?all_available=true&per_page=100&simple=true&order_by=last_activity_at`,
            { headers: { 'PRIVATE-TOKEN': token } }
        ).then(res => res.json());
    },
    getLatestBuild: (projectId, token = GitlabStore.get().token) => {
        console.log('Getting a build', projectId, token);
        return fetch(
            `https://gitlab.asynchrony.com/api/v4/projects/${projectId}/pipelines?&per_page=1`,
            { headers: { 'PRIVATE-TOKEN': token } }
        ).then(res => res.json())
        .then(builds => builds[0]);
    }
}