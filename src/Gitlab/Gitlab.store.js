import { Store } from "svelte/store";

class _GitlabStore extends Store {
}

export const GitlabStore = new _GitlabStore({
    token: loadField('token'),
    projects: loadJsonArrayField('projects'),
});

GitlabStore.on('state', ({ current, changed, previous }) => {
    if (changed.token) {
        localStorage.setItem('gitlabState_token', current.token);
    }

    if(changed.projects) {
        localStorage.setItem('gitlabState_projects', JSON.stringify(current.projects));
    }
});

function loadJsonArrayField(field) {
    return JSON.parse(localStorage.getItem('gitlabState_' + field) || '[]');
}

function loadField(field) {
    return localStorage.getItem('gitlabState_' + field);
}