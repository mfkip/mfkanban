var swPrecache = require('sw-precache');
var rootDir = 'public/';

swPrecache.write(`${rootDir}/sw.js`, {
    staticFileGlobs: [
        `${rootDir}**/*.{js,html,css,png,jpg,gif,svg,eot,ttf,woff}`,
    ],
    navigateFallback: '/index.html',
    stripPrefix: rootDir,
    maximumFileSizeToCacheInBytes: 2097152 * 4 // 8Mb
}, () => {
    console.log('sw generated');
});
